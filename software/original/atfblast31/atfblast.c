/* ATFBlast.exe - based on GALBLAST by Manfred Winterhoff
Compiled with Borland Turbo C++ for Windows V4.5

Changes:-
 2015-1-13  V0.1 bruce.abbott@xtra.co.nz
  -created from GALBLAST.C

 2017-10-23 V0.2
  - removed 6001-2, added ATF16V8 ATF22V10B,C

 2017-11-07 V0.3
  - bugfix: restore default GAL16V8/20V8 erase time (went missing in version 0.2).
  - parse ATF PES
  - can now change to detected gal type in TestProperGal (bug in original GALblast code!)

 2017-11-19 V0.31
  - ATF22V10B write 68 padding bits _before_ UES bits (same as ATF22V10C)

 */


#include <windows.h>
#include <stdio.h>
#include <ver.h>
#include <stdlib.h>
#include <stdarg.h>
#include <mmsystem.h>
#include <dos.h>
#include <time.h>
#include <ctype.h>
#include <string.h>
#include <commdlg.h>
#include <shellapi.h>

// contents of pes[3]
// Atmel PES is text string eg. 1B8V61F1 or 3Z01V22F1
//                                 ^           ^
#define LATTICE 0xA1
#define NATIONAL 0x8F
#define SGSTHOMSON 0x20
#define ATMEL16 'V'
#define ATMEL22 '3'

static char *caption[]={"Read GAL","Verify GAL","Read PES","Test SCLK","Write GAL","Erase GAL","Erase All","Burn Security","Write PES","VPP Test"};
#define READGAL 0
#define VERIFYGAL 1
#define READPES 2
#define SCLKTEST 3
#define WRITEGAL 4
#define ERASEGAL 5
#define ERASEALL 6
#define BURNSECURITY 7
#define WRITEPES 8
#define VPPTEST 9

#define READVPP 0   // Vpp = 12V
#define WRITEVPP 1  // Vpp = higher (eg. 14.5V)

typedef enum {UNKNOWN,GAL16V8,GAL20V8,GAL22V10,ATF16V8B,ATF22V10B,ATF22V10C} GALTYPE;

/* config bit numbers */
static int cfg16V8[]=
{
    2128,2129,2130,2131,2132,2133,2134,2135,2136,2137,2138,2139,2140,2141,2142,2143,2144,2145,2146,2147,2148,2149,2150,2151,2152,2153,2154,2155,2156,2157,2158,2159,
    2048,2049,2050,2051,
    2193,
    2120,2121,2122,2123,2124,2125,2126,2127,
    2192,
    2052,2053,2054,2055,
    2160,2161,2162,2163,2164,2165,2166,2167,2168,2169,2170,2171,2172,2173,2174,2175,2176,2177,2178,2179,2180,2181,2182,2183,2184,2185,2186,2187,2188,2189,2190,2191
};
static int cfg16V8AB[]=
{
    2048,2049,2050,2051,
    2193,
    2120,2121,2122,2123,
    2128,2129,2130,2131,2132,2133,2134,2135,2136,2137,2138,2139,2140,2141,2142,2143,2144,2145,2146,2147,2148,2149,2150,2151,2152,2153,2154,2155,2156,2157,2158,2159,2160,2161,2162,2163,2164,2165,2166,2167,2168,2169,2170,2171,2172,2173,2174,2175,2176,2177,2178,2179,2180,2181,2182,2183,2184,2185,2186,2187,2188,2189,2190,2191,
    2124,2125,2126,2127,
    2192,
    2052,2053,2054,2055
};
static int cfg20V8[]=
{
    2640,2641,2642,2643,2644,2645,2646,2647,2648,2649,2650,2651,2652,2653,2654,2655,2656,2657,2658,2659,2660,2661,2662,2663,2664,2665,2666,2667,2668,2669,2670,2671,
    2560,2561,2562,2563,
    2705,
    2632,2633,2634,2635,2636,2637,2638,2639,
    2704,
    2564,2565,2566,2567,
    2672,2673,2674,2675,2676,2677,2678,2679,2680,2681,2682,2683,2684,2685,2686,2687,2688,2689,2690,2691,2692,2693,2694,2695,2696,2697,2698,2699,2700,2701,2702,2703
};
static int cfg20V8AB[]=
{
    2560,2561,2562,2563,
    2705,
    2632,2633,2634,2635,
    2640,2641,2642,2643,2644,2645,2646,2647,2648,2649,2650,2651,2652,2653,2654,2655,2656,2657,2658,2659,2660,2661,2662,2663,2664,2665,2666,2667,2668,2669,2670,2671,2672,2673,2674,2675,2676,2677,2678,2679,2680,2681,2682,2683,2684,2685,2686,2687,2688,2689,2690,2691,2692,2693,2694,2695,2696,2697,2698,2699,2700,2701,2702,2703,
    2636,2637,2638,2639,
    2704,
    2564,2565,2566,2567
};
static int cfg22V10[]=
{
    5809,5808,
    5811,5810,
    5813,5812,
    5815,5814,
    5817,5816,
    5819,5818,
    5821,5820,
    5823,5822,
    5825,5824,
    5827,5826
};

/*
   UES     user electronic signature
   PES     programmer electronic signature (ATF = text string, others = Vendor/Vpp/timing)
   cfg     configuration bits for OLMCs
*/
/* GAL info */
static struct
{
    GALTYPE type;
    unsigned char id0,id1; /* variant 1, variant 2 (eg. 16V8=0x00, 16V8A+=0x1A)*/
    char *name;       /* pointer to chip name               */
    int fuses;        /* total number of fuses              */
    int pins;         /* number of pins on chip             */
    int rows;         /* number of fuse rows                */
    int bits;         /* number of fuses per row            */
    int uesrow;       /* UES row number                     */
    int uesfuse;      /* first UES fuse number              */
    int uesbytes;     /* number of UES bytes                */
    int eraserow;     /* row adddeess for erase             */
    int eraseallrow;  /* row address for erase all          */
    int pesrow;       /* row address for PES read/write     */
    int pesbytes;     /* number of PES bytes                */
    int cfgrow;       /* row address of config bits         */
    int *cfg;         /* pointer to config bit numbers      */
    int cfgbits;      /* number of config bits              */
}
galinfo[]=
{
    {UNKNOWN,   0x00,0x00,"unknown",     0, 0, 0,  0, 0,   0,0, 0, 0, 0, 8, 0,NULL,0},
    {GAL16V8,   0x00,0x1A,"GAL16V8",  2194,20,32, 64,32,2056,8,63,54,58, 8,60,cfg16V8AB,sizeof(cfg16V8AB)/sizeof(int)},
    {GAL20V8,   0x20,0x3A,"GAL20V8",  2706,24,40, 64,40,2568,8,63,59,58, 8,60,cfg20V8AB,sizeof(cfg20V8AB)/sizeof(int)},
    {GAL22V10,  0x48,0x49,"GAL22V10", 5892,24,44,132,44,5828,8,61,60,58,10,16,cfg22V10,sizeof(cfg22V10)/sizeof(int)},
    {ATF16V8B,  0x00,0x00,"ATF16V8B", 2194,20,32, 64,32,2056,8,63,54,58, 8,60,cfg16V8AB,sizeof(cfg16V8AB)/sizeof(int)},
    {ATF22V10B, 0x00,0x00,"ATF22V10B",5892,24,44,132,44,5828,8,61,60,58,10,16,cfg22V10,sizeof(cfg22V10)/sizeof(int)},
    {ATF22V10C, 0x00,0x00,"ATF22V10C",5892,24,44,132,44,5828,8,61,60,58,10,16,cfg22V10,sizeof(cfg22V10)/sizeof(int)},
};

#define MAXFUSES 10000
#define GALBUFSIZE 16384

static HANDLE hInstance;
static HICON appicon;
static int vppmul=128;
static char progname[]="ATFblast";
static short far *lptbase=(short far *)0x00400008L;
static short lptport[3];
static int duration[16]={1,2,5,10,20,30,40,50,60,70,80,90,100,200,0,0};
static int lpt=0,security=0,erasetime=0,progtime=0,vpp=0;
static GALTYPE gal;
static char fusemap[MAXFUSES];
static char backup[MAXFUSES];
static unsigned short checksum;
static unsigned char pes[12];
static char galbuffer[GALBUFSIZE];
static unsigned char port;
static unsigned char control=0x04;
static BOOL writeorerase=0;
static HWND mainwindow;


static int Message(HWND wnd,LPSTR format,LPSTR caption,int flags,...)
{
    static char buffer[1024];
    va_list arg;

    va_start(arg,flags);
    wvsprintf(buffer,format,arg);
    va_end(arg);
    if(!caption) caption=progname;
    MessageBeep(flags&MB_ICONMASK);
    return MessageBox(wnd,buffer,caption,flags);
}

static void WaitCursor(BOOL on)
{
    POINT p;
    HWND w;
    HCURSOR h;

    if(on)
    {
        SetCursor(LoadCursor(0,IDC_WAIT));
    }
    else
    {
        GetCursorPos(&p);
        w=WindowFromPoint(p);
        h=w?GetClassWord(w,GCW_HCURSOR):0;
        SetCursor(h?h:LoadCursor(0,IDC_ARROW));
    }
}

static unsigned short CheckSum(int n)
{
    unsigned short c,e;
    long a;
    int i;

    c=e=0;
    a=0;
    for(i=0;i<n;i++)
    {
        e++;
        if(e==9)
    {
        e=1;
        a+=c;
        c=0;
    }
        c>>=1;
    if(fusemap[i]) c+=0x80;
    }
    return (unsigned short)((c>>(8-e))+a);
}

static int ParseFuseMap(HWND wnd,char *ptr)
{
    int i,n,type,checksumpos,address,pins,lastfuse;
    int state; // 0=outside JEDEC, 1=skipping comment or unknown, 2=read command

    state=0;
    security=0;
    checksum=0;
    checksumpos=0;
    pins=0;
    lastfuse=0;
    for(n=0;ptr[n];n++)
    {
   if(ptr[n]=='*') state=2; // else ignored
   else switch(state)
   {
   case 2:
       if(!isspace(ptr[n])) switch(ptr[n])
       {
       case 'L':
          address=0;
          state=3;
          break;
       case 'F':
          state=5;
          break;
       case 'G':
          state=13;
          break;
       case 'Q':
          state=7;
          break;
       case 'C':
          checksumpos=n;
          state=14;
          break;
       default:
          state=1;
       }
       break;
   case 3:
       if(!isdigit(ptr[n])) return n;
       address=ptr[n]-'0';
       state=4;
       break;
   case 4:
       if(isspace(ptr[n]))
       {
           state=6;
       }
       else if(isdigit(ptr[n]))
       {
           address=10*address+(ptr[n]-'0');
       }
       else
       {
           return n;
       }
       break;
   case 5:
       if(isspace(ptr[n])) break; // ignored
       if(ptr[n]=='0'||ptr[n]=='1')
       {
           memset(fusemap,ptr[n]-'0',sizeof(fusemap));
       }
       else
       {
           return n;
       }
       state=1;
       break;
   case 6:
       if(isspace(ptr[n])) break; // ignored
       if(ptr[n]=='0'||ptr[n]=='1')
       {
           fusemap[address++]=ptr[n]-'0';
       }
       else
       {
           return n;
       }
       break;
   case 7:
       if(isspace(ptr[n])) break; // ignored
       if(ptr[n]=='P')
       {
           pins=0;
           state=8;
       }
       else if(ptr[n]=='F')
       {
           lastfuse=0;
           state=9;
       }
       else state=2;
       break;
   case 8:
       if(isspace(ptr[n])) break; // ignored
       if(!isdigit(ptr[n])) return n;
       pins=ptr[n]-'0';
            state=10;
       break;
   case 9:
       if(isspace(ptr[n])) break; // ignored
            if(!isdigit(ptr[n])) return n;
       lastfuse=ptr[n]-'0';
       state=11;
            break;
        case 10:
            if(isdigit(ptr[n]))
            {
                pins=10*pins+(ptr[n]-'0');
            }
            else if(isspace(ptr[n]))
            {
                state=12;
            }
            else return n;
            break;
   case 11:
            if(isdigit(ptr[n]))
            {
                lastfuse=10*lastfuse+(ptr[n]-'0');
            }
            else if(isspace(ptr[n]))
            {
                state=12;
            }
            else return n;
            break;
   case 12:
            if(!isspace(ptr[n])) return n;
       break;
   case 13:
       if(isspace(ptr[n])) break; // ignored
       if(ptr[n]=='0'||ptr[n]=='1')
       {
           security=ptr[n]-'0';
       }
       else
       {
           return n;
       }
       state=1;
       break;
   case 14:
       if(isspace(ptr[n])) break; // ignored
       if(isdigit(ptr[n]))
       {
           checksum=ptr[n]-'0';
       }
       else if(toupper(ptr[n])>='A'&&toupper(ptr[n])<='F')
       {
           checksum=toupper(ptr[n])-'A'+10;
       }
       else return n;
       state=15;
       break;
   case 15:
       if(isdigit(ptr[n]))
       {
           checksum=16*checksum+ptr[n]-'0';
       }
       else if(toupper(ptr[n])>='A'&&toupper(ptr[n])<='F')
       {
           checksum=16*checksum+toupper(ptr[n])-'A'+10;
       }
       else if(isspace(ptr[n]))
       {
           state=2;
       }
       else return n;
       break;
    }
    }
    if(lastfuse||pins)
    {
        if(checksum&&checksum!=CheckSum(lastfuse))
        {
            if(Message(wnd,"Checksum given %04X calculated %04X",NULL,MB_OKCANCEL,checksum,CheckSum(lastfuse))==IDCANCEL)
            {
                return checksumpos;
            }
        }
        for(type=0,i=1;i<sizeof(galinfo)/sizeof(galinfo[0]);i++)
        {
            if((lastfuse==0||galinfo[i].fuses==lastfuse||galinfo[i].uesfuse==lastfuse&&galinfo[i].uesfuse+8*galinfo[i].uesbytes==galinfo[i].fuses)
            && (pins==0||galinfo[i].pins==pins||galinfo[i].pins==24&&pins==28))
            {
                if(gal==0)
                {
                     type=i;
                     break;
                }
                else if(!type)
                {
                     type=i;
                }
            }
        }
    }
    return n;
}

static BOOL CheckJEDEC(HWND wnd)
{
    int position;

    position=ParseFuseMap(wnd,galbuffer);
    if(position==strlen(galbuffer)) return TRUE;
    Message(wnd,"Error in JEDEC",NULL,MB_OK|MB_ICONEXCLAMATION);
    SendDlgItemMessage(wnd,102,EM_SETSEL,0,MAKELONG(position,position));
    SendMessage(wnd,WM_NEXTDLGCTL,GetDlgItem(wnd,102),TRUE);
    return FALSE;
}

static void Delay(int msec)
{
    long start=timeGetTime();
    while(timeGetTime()<start+msec) ;
}

void output(int port,unsigned char value)
{
    int last1,last2;

    last1=inportb(0x40);
    last2=inportb(0x40);
    while(last1==inportb(0x40)&&last2==inportb(0x40)) ;
    outportb(port,value);
}

unsigned char input(int port)
{
    int last1,last2;

    last1=inportb(0x40);
    last2=inportb(0x40);
    while(last1==inportb(0x40)&&last2==inportb(0x40)) ;
    return inportb(port);
}

/* set/reset individual pins of LPT port */
static void SetSTROBE(BOOL on)
{
    output(lptport[lpt]+2,control=(on?control&~0x01:control|0x01));
}

static void SetFEED(BOOL on)
{
    output(lptport[lpt]+2,control=(on?control&~0x02:control|0x02));
}

static void SetINIT(BOOL on)
{
    output(lptport[lpt]+2,control=(on?control|0x04:control&~0x04));
}

static void SetSEL(BOOL on)
{
    output(lptport[lpt]+2,control=(on?control&~0x08:control|0x08));
}

/* output row address (RA0-5) to parallel port */
static void SetRow(int row)
{
    port=(port&0x81)|(row<<1);
    output(lptport[lpt],port);
}

static BOOL GetACK(void)
{
    return (input(lptport[lpt]+1)&0x40)!=0;
}

/* set/reset individual pins of GAL */
static void SetVCC(BOOL on)
{
    SetINIT(!on);
}

static void SetVPP(BOOL on)
{
    if (on){
      SetFEED(1);  // FEED Low = programming voltage
    }
    else {
      SetFEED(0);  // FEED high = +12V
    }
}

static void SetSDIN(BOOL on)
{
    if(on)
    {
   port|=0x01;
    }
    else
    {
   port&=0xFE;
    }
    output(lptport[lpt],port);
}

static void SetSTB(BOOL on)
{
    SetSTROBE(on);
}

static void SetPV(BOOL on)
{
    SetSEL(on);
}

static void SetSCLK(BOOL on)
{
    if(on)
    {
   port|=0x80;
    }
    else
    {
   port&=0x7F;
    }
    output(lptport[lpt],port);
}

static BOOL GetSDOUT(void)
{
    return GetACK();
}

// turn off power
static void TurnOff(void)
{
    Delay(100);
    SetPV(0);    // P/V- low
    SetRow(0x3F);// RA0-5 high
    SetSDIN(1);  // SDIN high
    SetVPP(0);   // Vpp off (+12V)
    SetPV(1);    // P/V- high
    Delay(2);
    SetVCC(0);   // turn off VCC (if controlled)
    WaitCursor(FALSE);
}

BOOL WINAPI GrayDlgProc(HWND wnd,unsigned msg,WPARAM wParam,LPARAM lParam)
{
   RECT r;
   PAINTSTRUCT ps;

   switch(msg)
   {
   case WM_INITDIALOG:
   return TRUE;
   case WM_COMMAND:
   switch(LOWORD(wParam))
   {
   case IDCANCEL:
       EndDialog(wnd,FALSE);
       return TRUE;
   }
   break;
   case WM_CTLCOLOR:
   if(HIWORD(lParam)==CTLCOLOR_STATIC)
   {
       SetBkMode((HDC)wParam,TRANSPARENT);
       return GetStockObject(LTGRAY_BRUSH);
   }
   return FALSE;
   case WM_ERASEBKGND:
   if(IsIconic(wnd)) return TRUE;
   GetClientRect(wnd,&r);
   FillRect((HDC)wParam,&r,GetStockObject(LTGRAY_BRUSH));
   SetWindowLong(wnd,DWL_MSGRESULT,1);
   return TRUE;
   case WM_PAINT:
   if(IsIconic(wnd))
   {
       BeginPaint(wnd,&ps);
       DefWindowProc(wnd,WM_ICONERASEBKGND,(WORD)ps.hdc,0L);
       DrawIcon(ps.hdc,0,0,appicon);
       EndPaint(wnd,&ps);
       return TRUE;
   }
   return FALSE;
   case WM_QUERYDRAGICON:
   return appicon;
   }
   return FALSE;
}

BOOL WINAPI _export AssureDlgProc(HWND wnd,unsigned msg,WPARAM wParam,LPARAM lParam)
{
    int i,j,n;
    char buffer[128];
    static int useerase;

    switch(msg)
    {
    case WM_INITDIALOG:
        SetWindowText(wnd,caption[lParam]);
        SetDlgItemText(wnd,IDOK,caption[lParam]);
        if (gal==ATF16V8B || gal==ATF22V10B || gal==ATF22V10C)
        {
            strcpy(buffer, "  Atmel ");
        }
        else
        {
           if(pes[1]&0x10)
           {
               strcpy(buffer,"3.3V");
           }
           else
           {
               strcpy(buffer,"5V");
           }
           switch(pes[3])
           {
           case LATTICE:
               strcat(buffer," Lattice ");
               break;
           case NATIONAL:
               strcat(buffer," National ");
               break;
           case SGSTHOMSON:
               strcat(buffer," ST ");
           break;
           default:
               strcat(buffer," Unknown ");
        }
    }
    strcat(buffer,galinfo[gal].name);
    SetDlgItemText(wnd,100,buffer);
    j=wsprintf(buffer,"PES:");
    for(i=0;i<galinfo[gal].pesbytes;i++)
    {
       j+=wsprintf(buffer+j," %02X",pes[i]);
    }
    SetDlgItemText(wnd,103,buffer);
    wsprintf(buffer,"VPP=%d.%02dV Prog-Pulse=%dms Erase-Pulse=%dms",vpp/4,(vpp%4)*25,progtime,erasetime);
    SetDlgItemText(wnd,104,buffer);
    n=-1;
    for(i=48;i<83;i++)
    {
       wsprintf(buffer,"%d.%02d",i/4,(i%4)*25);
       j=(int)SendDlgItemMessage(wnd,101,CB_ADDSTRING,0,(LPARAM)(LPSTR)buffer);
       SendDlgItemMessage(wnd,101,CB_SETITEMDATA,j,i);
       if(i==vpp) n=j;
    }
    SendDlgItemMessage(wnd,101,CB_SETCURSEL,n,0l);
    n=-1;
    if(lParam==ERASEGAL||lParam==ERASEALL)
    {
        useerase=TRUE;
        SetDlgItemText(wnd,99,"Erase-Pulse");
        for(i=25;i<=800;i<<=1)
        {
            wsprintf(buffer,"%d",i/2);
            j=(int)SendDlgItemMessage(wnd,102,CB_ADDSTRING,0,(LPARAM)(LPSTR)buffer);
            SendDlgItemMessage(wnd,102,CB_SETITEMDATA,j,i/2);
            if(i/2==erasetime) n=j;
        }
    }
    else
    {
        useerase=FALSE;
        SetDlgItemText(wnd,99,"Prog-Pulse");
        for(i=0;i<14;i++)
        {
            wsprintf(buffer,"%d",duration[i]);
            j=(int)SendDlgItemMessage(wnd,102,CB_ADDSTRING,0,(LPARAM)(LPSTR)buffer);
            SendDlgItemMessage(wnd,102,CB_SETITEMDATA,j,duration[i]);
            if(duration[i]==progtime) n=j;
        }
    }
    SendDlgItemMessage(wnd,102,CB_SETCURSEL,n,0l);
    return TRUE;
    case WM_COMMAND:
        switch(LOWORD(wParam))
        {
        case IDOK:
       i=(int)SendDlgItemMessage(wnd,101,CB_GETCURSEL,0,0L);
       if(i==-1)
            {
                Message(wnd,"Select VPP first",progname,MB_ICONEXCLAMATION|MB_OK);
                SetFocus(GetDlgItem(wnd,101));
                return TRUE;
            }
            vpp=(int)SendDlgItemMessage(wnd,101,CB_GETITEMDATA,i,0L);
            i=(int)SendDlgItemMessage(wnd,102,CB_GETCURSEL,0,0L);
       if(i==-1)
            {
                GetDlgItemText(wnd,99,buffer,sizeof(buffer));
                Message(wnd,"Select %s first",progname,MB_ICONEXCLAMATION|MB_OK,(LPSTR)buffer);
                SetFocus(GetDlgItem(wnd,102));
                return TRUE;
            }
            if(useerase)
            {
                erasetime=(int)SendDlgItemMessage(wnd,102,CB_GETITEMDATA,i,0L);
            }
            else
            {
      progtime=(int)SendDlgItemMessage(wnd,102,CB_GETITEMDATA,i,0L);
            }
            EndDialog(wnd,1);
            return TRUE;
        case IDCANCEL:
            EndDialog(wnd,0);
       return TRUE;
        }
    }
    return GrayDlgProc(wnd,msg,wParam,lParam);
}

// turn on power
static BOOL TurnOn(HWND wnd,int mode)
{
    if(mode==WRITEGAL||mode==ERASEGAL||mode==ERASEALL||mode==BURNSECURITY||mode==WRITEPES||mode==VPPTEST)
    {
        if(DialogBoxParam(hInstance,MAKEINTRESOURCE(4),wnd,AssureDlgProc,mode)!=1) return FALSE;
        writeorerase=TRUE;
    }
    else
    {
        writeorerase=FALSE;
    }
    WaitCursor(TRUE);
    SetVPP(0);    // VPP off
    SetPV(0);     // P/V- low
    SetRow(0x3F); // RA0-5 high
    SetSDIN(1);   // SDIN high
    SetSCLK(1);   // SCLK high
    SetSTB(1);    // STB high
    SetVCC(1);    // turn on VCC (if controlled)
    Delay(100);
    SetSCLK(0);   // SCLK low
    if (writeorerase)
    {
        SetVPP(1); // VPP = programming voltage
    }
    else
    {
        SetVPP(0); // VPP = +12V
    }
    Delay(20);
    return TRUE;
}

// clock bit in from SDOUT
static BOOL ReceiveBit(void)
{
    BOOL bit;
    bit=GetSDOUT();
    SetSCLK(1);
    SetSCLK(0);
    return bit;
}

static void DiscardBits(int n)
{
    while(n-- >0) ReceiveBit();
}

/* clock bit out to SDIN */
static void SendBit(int bit)
{
    SetSDIN(bit);
    SetSCLK(1);
    SetSCLK(0);
}

static void SendBits(int n,int bit)
{
    while(n-- >0) SendBit(bit);
}

/* send row address bits to SDIN */
/* ATF22V10C MSb first, other 22V10 LSb first */
static void SendAddress(int n, int row)
{
    switch (gal)
    {
    case ATF22V10C:
        while(n-- >1)
        {
            SendBit(row&32);   // clock in row number bits 5-1
            row<<=1;
        }
        SetSDIN(row&32);       // SDIN = row number bit 0
        break;
    default:
        while(n-- >0)
        {
            SendBit(row&1);    // clock in row number bits 0-5
            row>>=1;
        }
        SetSDIN(0); // SDIN = low
    }
}

/* pulse /STB low for some milliseconds */
static void Strobe(int msec)
{
//    Delay(2);
    SetSTB(0);
    Delay(msec);
    SetSTB(1);
//    Delay(2);
}

// 16V8, 20V8 RA0-5 = row address, strobe.
// 22V10 RA0-5 = 0, send row address (6 bits), strobe.
static void StrobeRow(int row)
{
    switch(gal)
    {
    case GAL16V8:
    case GAL20V8:
    case ATF16V8B:
       SetRow(row);         // set RA0-5 to row number
       Strobe(2);           // pulse /STB for 2ms
        break;
    case GAL22V10:
    case ATF22V10B:
    case ATF22V10C:
       SetRow(0);           // set RA0-5 low
       SendAddress(6,row);  // send row number (6 bits)
       SetSTB(0);
       SetSTB(1);           // pulse /STB
       SetSDIN(0);          // SDIN low
    }
}

static BOOL GetSetup(HWND wnd)
{
    int i;

    i=(int)SendDlgItemMessage(wnd,100,CB_GETCURSEL,0,0L);
    if(i==-1)
    {
   Message(wnd,"Select LPT port first",NULL,MB_OK|MB_ICONEXCLAMATION);
   return FALSE;
    }
    lpt=(int)SendDlgItemMessage(wnd,100,CB_GETITEMDATA,i,0L);
    i=(int)SendDlgItemMessage(wnd,101,CB_GETCURSEL,0,0L);
    if(i==-1)
    {
   Message(wnd,"Select a GAL type first",NULL,MB_OK|MB_ICONEXCLAMATION);
   return FALSE;
    }
    gal=(int)SendDlgItemMessage(wnd,101,CB_GETITEMDATA,i,0L);
    return TRUE;
}

static void GetPES(char pes[])
{
    int bitmask,byte;

    StrobeRow(galinfo[gal].pesrow);
    for(byte=0;byte<galinfo[gal].pesbytes;byte++)
        {
        pes[byte]=0;
        for(bitmask=0x1;bitmask<=0x80;bitmask<<=1)
            {
            if(ReceiveBit()) pes[byte]|=bitmask;
        }
    }
}

static void ReadPES(HWND wnd,char pes[])
{
    TurnOn(wnd,READPES);
    GetPES(pes);
    TurnOff();
}

static void WritePES(HWND wnd,char pes[])
{
    int byte,bitmask;

    if(TurnOn(wnd,WRITEPES))
    {
        switch(gal)
        {
        case GAL16V8:
        case GAL20V8:
        case ATF16V8B:
            SetPV(1);
            SetRow(galinfo[gal].pesrow);
            for(byte=0;byte<8;byte++) for(bitmask=0x01;bitmask<=0x80;bitmask<<=1) SendBit((pes[byte]&bitmask)!=0);
            Strobe(progtime);
            SetPV(0);
            break;
        case GAL22V10:
        case ATF22V10B:
        case ATF22V10C:
            SetRow(0);
            for(byte=0;byte<galinfo[gal].pesbytes;byte++) for(bitmask=0x01;bitmask<=0x80;bitmask<<=1) SendBit((pes[byte]&bitmask)!=0);
            if(galinfo[gal].pesbytes*8<galinfo[gal].bits) SendBits(galinfo[gal].bits-galinfo[gal].pesbytes*8,0);
            SendAddress(6,galinfo[gal].pesrow);
            SetSDIN(0);
            SetPV(1);
            Strobe(progtime);
            SetPV(0);
        }
        TurnOff();
    }
}

static void WriteGAL(HWND wnd,char *fuses)
{
    int row,bit;

    if(TurnOn(wnd,WRITEGAL))
    {
        switch(gal)
        {
        case GAL16V8:
        case GAL20V8:
            SetPV(1);
            // write fuse array
            for(row=0;row<galinfo[gal].rows;row++)
            {
                SetRow(row);
                for(bit=0;bit<galinfo[gal].bits;bit++)
                {
                    SendBit(fuses[galinfo[gal].rows*bit+row]);
                }
                Strobe(progtime);
            }
            // write UES
            SetRow(galinfo[gal].uesrow);
            for(bit=0;bit<64;bit++)
            {
                SendBit(fuses[galinfo[gal].uesfuse+bit]);
            }
            Strobe(progtime);
            // write CFG
            SetRow(galinfo[gal].cfgrow);
            for(bit=0;bit<82;bit++)
            {
                switch(pes[2])
                {
                case 0x00:
                    SendBit(fuses[cfg16V8[bit]]);
                    break;
                case 0x1A:
                    SendBit(fuses[cfg16V8AB[bit]]);
                    break;
                case 0x20:
                    SendBit(fuses[cfg20V8[bit]]);
                    break;
                case 0x3A:
                    SendBit(fuses[cfg20V8AB[bit]]);
                    break;
                }
            }
            Strobe(progtime);
            SetPV(0);
            break;
        case ATF16V8B:
            SetPV(1);
            // write fuse array
            for(row=0;row<galinfo[gal].rows;row++)
            {
                SetRow(row);
                for(bit=0;bit<galinfo[gal].bits;bit++)
                {
                    SendBit(fuses[galinfo[gal].rows*bit+row]);
                }
                Strobe(progtime);
            }
            // write UES
            SetRow(galinfo[gal].uesrow);
            for(bit=0;bit<64;bit++)
            {
                SendBit(fuses[galinfo[gal].uesfuse+bit]);
            }
            Strobe(progtime);
            // write CFG
            SetRow(galinfo[gal].cfgrow);
            for(bit=0;bit<82;bit++)
            {
                SendBit(fuses[cfg16V8AB[bit]]);
            }
            Strobe(progtime);
            SetPV(0);
            break;
        case GAL22V10:
        case ATF22V10B:
            // write fuse array
            SetRow(0);               // RA0-5 low
            for(row=0;row<galinfo[gal].rows;row++)
            {
                for(bit=0;bit<galinfo[gal].bits;bit++)
                {
                    SendBit(fuses[galinfo[gal].rows*bit+row]);
                }
                SendAddress(6,row);  // send address 6 bits
                SetPV(1);
                Strobe(progtime);
                SetPV(0);
            }
            // write UES
            if(gal==ATF22V10B)
            {
               SendBits(68,1);                              // fill first 68 bits
               for(bit=0;bit<64;bit++)
               {
                  SendBit(fuses[galinfo[gal].uesfuse+bit]); // send UES bits
               }
            }
            else
            {
               for(bit=0;bit<64;bit++)
               {
                  SendBit(fuses[galinfo[gal].uesfuse+bit]); // send UES bits
               }
               SendBits(68,0);                              // fill next 68 bits
            }
            SendAddress(6,galinfo[gal].uesrow);
            SetPV(1);
            Strobe(progtime);
            SetPV(0);
            // write CFG
            SetRow(galinfo[gal].cfgrow);
            for(bit=0;bit<galinfo[gal].cfgbits;bit++)
            {
                SendBit(fuses[galinfo[gal].cfg[bit]]);
            }
            SetPV(1);
            Strobe(progtime);
            SetPV(0);
            break;
        case ATF22V10C:
            SetRow(0);      // RA0-5 low
            for(row=0;row<galinfo[gal].rows;row++)
            {
                for(bit=0;bit<galinfo[gal].bits;bit++)
                {
                    SendBit(fuses[galinfo[gal].rows*bit+row]);
                }
                SendAddress(6,row);  // send address 6 bits
                SetPV(1);
                Strobe(progtime);
                SetPV(0);
            }
            // write UES
            SendBits(68,1);
            for(bit=0;bit<64;bit++)
            {
                SendBit(fuses[galinfo[gal].uesfuse+bit]);
            }
            SendAddress(6,galinfo[gal].uesrow);
            SetPV(1);
            Strobe(progtime);
            SetPV(0);
            // write CFG
            SetRow(galinfo[gal].cfgrow);
            for(bit=0;bit<19;bit++)
            {
                SendBit(fuses[galinfo[gal].cfg[bit]]); // clock out bits 0-62
            }
            SetSDIN(fuses[galinfo[gal].cfg[19]]); // send bit 63
            SetPV(1);
            Strobe(progtime);
            SetPV(0);
            // disable power-down feature (JEDEC bit #5892)
            SetRow(0);
            SendAddress(6,59);
            SetPV(1);
            Strobe(progtime);
            SetPV(0);
        }
    TurnOff();
    }
}

// read fuse rows, UES, CFG
static void ReadGAL(HWND wnd,char *fuses)
{
    int row,bit;

    TurnOn(wnd,READGAL);
    switch(gal)
    {
    case GAL16V8:
    case GAL20V8:
    // read fuse rows
    for(row=0;row<galinfo[gal].rows;row++)
    {
       StrobeRow(row);
       for(bit=0;bit<galinfo[gal].bits;bit++)
       {
      fuses[galinfo[gal].rows*bit+row]=ReceiveBit();
       }
    }
    // read UES
    StrobeRow(galinfo[gal].uesrow);
    for(bit=0;bit<64;bit++)
    {
       fuses[galinfo[gal].uesfuse+bit]=ReceiveBit();
    }
    // read CFG
    StrobeRow(galinfo[gal].cfgrow);
    for(bit=0;bit<82;bit++)
    {
       switch(pes[2])
       {
       case 0x00:
           fuses[cfg16V8[bit]]=ReceiveBit();
           break;
       case 0x1A:
           fuses[cfg16V8AB[bit]]=ReceiveBit();
           break;
       case 0x20:
           fuses[cfg20V8[bit]]=ReceiveBit();
           break;
       case 0x3A:
           fuses[cfg20V8AB[bit]]=ReceiveBit();
           break;
       }
    }
    case ATF16V8B:
        // read fuse rows ATF16V8
        for(row=0;row<galinfo[gal].rows;row++)
        {
           StrobeRow(row);
           for(bit=0;bit<galinfo[gal].bits;bit++)
           {
               fuses[galinfo[gal].rows*bit+row]=ReceiveBit();
           }
        }
        // read UES ATF16V8
        StrobeRow(galinfo[gal].uesrow);
        for(bit=0;bit<64;bit++)
        {
            fuses[galinfo[gal].uesfuse+bit]=ReceiveBit();
        }
        // read CFG ATF16V8
        StrobeRow(galinfo[gal].cfgrow);
        for(bit=0;bit<82;bit++)
        {
            fuses[galinfo[gal].cfg[bit]]=ReceiveBit();
        }
        break;
    case GAL22V10:
    case ATF22V10B:
    case ATF22V10C:
        // read fuse rows
        for(row=0;row<galinfo[gal].rows;row++)
        {
           StrobeRow(row);
           for(bit=0;bit<galinfo[gal].bits;bit++)
           {
               fuses[galinfo[gal].rows*bit+row]=ReceiveBit();
           }
        Delay(1);
        }
        // read UES
        StrobeRow(galinfo[gal].uesrow);
        if (gal==GAL22V10)
        {
            for(bit=0;bit<64;bit++)
            {
                fuses[galinfo[gal].uesfuse+bit]=ReceiveBit();
            }
        }
        else // ATF22V10x
        {
            DiscardBits(68);
            for(bit=0;bit<64;bit++)
            {
                fuses[galinfo[gal].uesfuse+bit]=ReceiveBit();
            }
        }
        Delay(1);
        // read CFG
        SetRow(galinfo[gal].cfgrow);
        Strobe(1);
        for(bit=0;bit<galinfo[gal].cfgbits;bit++)
        {
            fuses[galinfo[gal].cfg[bit]]=ReceiveBit();
        }
    }
    TurnOff();
}

static void EraseGAL(HWND wnd,int gal)
{
    if(TurnOn(wnd,ERASEGAL))
    {
        SetPV(1);
        SetRow(galinfo[gal].eraserow);
        if(gal==GAL16V8||gal==ATF16V8B||gal==GAL20V8)
        {
            SendBit(1);
        }
        Strobe(erasetime);
        SetPV(0);
        TurnOff();
    }
}

static void EraseWholeGAL(HWND wnd,int gal)
{
    if(TurnOn(wnd,ERASEALL))
    {
        SetRow(galinfo[gal].eraseallrow);
        SetPV(1);
        if(gal==GAL16V8||gal==ATF16V8B||gal==GAL20V8)
        {
            SendBit(1);
        }
        Strobe(erasetime);
        SetPV(0);
        TurnOff();
    }
}

static void BurnSecurity(HWND wnd,int gal)
{
    if(TurnOn(wnd,BURNSECURITY))
    {
        switch(gal)
        {
        case GAL16V8:
        case GAL20V8:
        case ATF16V8B:
            SetPV(1);
            SetRow(61);
            SendBit(1);
        Strobe(progtime);
            SetPV(0);
        break;
        case GAL22V10:
        case ATF22V10B:
        case ATF22V10C:
            SetRow(0);
            SendBits(132,0);
            SendAddress(6,61);
            SetSDIN(0);
            SetPV(1);
            Strobe(progtime);
            SetPV(0);
        }
        TurnOff();
    }
}

static void FormatJEDEC(int gal,char *fuses,char *buffer)
{
    int i,j,k,n;
    int unused,start;
    time_t now;
    unsigned char ch;

    time(&now);
    n=wsprintf(buffer,"JEDEC file for %s created on %s",(LPSTR)galinfo[gal].name,(LPSTR)asctime(localtime(&now)))-1;
    n+=wsprintf(buffer+n,"\r\n*QP%d*QF%d*QV0*F0*G0*X0*\r\n",galinfo[gal].pins,galinfo[gal].fuses);
    for(i=k=0;i<galinfo[gal].bits;i++)
    {
        start=n;
        unused=TRUE;
        n+=wsprintf(buffer+n,"L%04d ",k);
        for(j=0;j<galinfo[gal].rows;j++)
        {
            if(fuses[k]) unused=FALSE;
            buffer[n++]='0'+fuses[k++];
        }
        n+=wsprintf(buffer+n,"*\r\n");
        if(unused) n=start;
    }
    if(k<galinfo[gal].uesfuse)
    {
        start=n;
        unused=TRUE;
        n+=wsprintf(buffer+n,"L%04d ",k);
        while(k<galinfo[gal].uesfuse)
        {
             if(fuses[k]) unused=FALSE;
             buffer[n++]='0'+fuses[k++];
        }
        n+=wsprintf(buffer+n,"*\r\n");
        if(unused) n=start;
    }
    start=n;
    unused=TRUE;
    n+=wsprintf(buffer+n,"N UES");
    for(j=0;j<galinfo[gal].uesbytes;j++)
    {
        ch=0;
        for(i=0;i<8;i++)
        {
            if(fuses[k+8*j+i])
            {
                if (gal==ATF22V10C)
                {
                    ch|=1<<(7-i);  // big-endian
                }
                else
                {
                    ch|=1<<i;     // little-endian
                }
            }
        }
        n+=wsprintf(buffer+n," %02X",ch);
    }
    n+=wsprintf(buffer+n,"*\r\nL%04d ",k);
    for(j=0;j<8*galinfo[gal].uesbytes;j++)
    {
        if(fuses[k]) unused=FALSE;
        buffer[n++]='0'+fuses[k++];
    }
    n+=wsprintf(buffer+n,"*\r\n");
    if(unused) n=start;
    if(k<galinfo[gal].fuses)
    {
       start=n;
       unused=TRUE;
       n+=wsprintf(buffer+n,"L%04d ",k);
       while(k<galinfo[gal].fuses)
       {
            if(fuses[k]) unused=FALSE;
            buffer[n++]='0'+fuses[k++];
       }
       n+=wsprintf(buffer+n,"*\r\n");
       if(unused) n=start;
    }
    n+=wsprintf(buffer+n,"N PES");
    for(i=0;i<galinfo[gal].pesbytes;i++)
    {
        n+=wsprintf(buffer+n," %02X",pes[i]);
    }
    n+=wsprintf(buffer+n,"*\r\nC%04X\r\n*",CheckSum(galinfo[gal].fuses));
    buffer[n]='\0';
}

static char *compare(char *buffer,char *ptr,int bytes)
{
    while(bytes-->0)
    {
        if(*buffer!=*ptr) return buffer;
        buffer++;
        ptr++;
    }
    return NULL;
}

static int hex(char *buffer)
{
    char *end;
    unsigned long value;

    value=strtoul(buffer,&end,16);
    if(value>255||*end!='\0') return -1;
    return (int)value;
}

BOOL WINAPI AboutDlgProc(HWND wnd,unsigned msg,WPARAM wParam,LPARAM lParam)
{
    DWORD size,handle;
    UINT len;
    char buffer[_MAX_PATH];
    void FAR *ptr;
    char *mem;

    switch(msg)
    {
    case WM_INITDIALOG:
        GetModuleFileName(hInstance,buffer,sizeof(buffer));
        size=GetFileVersionInfoSize(buffer,&handle);
        if(size)
        {
            mem=malloc(size);
            if(mem)
            {
                if(GetFileVersionInfo(buffer,0L,size,mem))
                {
                    if(VerQueryValue(mem,"\\StringFileInfo\\040904E4\\FileDescription",&ptr,&len)&&len&&ptr)
                    {
                         SetDlgItemText(wnd,100,ptr);
                    }
                    if(VerQueryValue(mem,"\\StringFileInfo\\040904E4\\FileVersion",&ptr,&len)&&len&&ptr)
                    {
                         SetDlgItemText(wnd,101,ptr);
                    }
                    if(VerQueryValue(mem,"\\StringFileInfo\\040904E4\\LegalCopyright",&ptr,&len)&&len&&ptr)
                    {
                         SetDlgItemText(wnd,102,ptr);
                    }
                    if(VerQueryValue(mem,"\\StringFileInfo\\040904E4\\CompanyName",&ptr,&len)&&len&&ptr)
                    {
                         SetDlgItemText(wnd,103,ptr);
                    }
                }
                free(mem);
            }
        }
        break;
    }
    return GrayDlgProc(wnd,msg,wParam,lParam);
}

static void ParsePES(void)
{
    int algo;

    switch (gal)
    {
    case ATF16V8B:
    case ATF22V10B:
    case ATF22V10C:
        progtime=10;
        erasetime=100;
        vpp=48;    /* 12.0V */
    break;
    default:
        algo=pes[1]&0x0F;
        if(algo==5)
        {
            erasetime=(25<<((pes[4]>>2)&7))/2;
            progtime=duration[((((unsigned)pes[5]<<8)|pes[4])>>5)&15];
            vpp=2*((pes[5]>>1)&31)+20;
        }
        else switch(gal)
        {
        case GAL16V8:
        case GAL20V8:
        erasetime=100;
            switch(algo)
            {
            case 0:
                vpp=63; // 15.75V
                progtime=100;
                break;
            case 1:
                vpp=63; // 15.75V
                progtime=80;
                break;
            case 2:
                vpp=66; // 16.5V
                progtime=10;
                break;
            case 3:
                vpp=pes[3]==NATIONAL?60:58; // 15.0V or 14.5V
                progtime=40;
                break;
            case 4:
                vpp=56; // 14V
                progtime=100;
                break;
            }
            break;
        default:
            erasetime=pes[3]==NATIONAL?50:100;
            switch(algo)
            {
            case 0:
                vpp=66; // 16.5V
                progtime=10;
                break;
            case 1:
                vpp=63; // 15.75V
                progtime=100;
                break;
            case 2:
                vpp=pes[3]==NATIONAL?60:58; // 15.0V or 14.5V
                progtime=40;
                break;
            case 3:
                vpp=56; // 14V
                progtime=100;
                break;
            }
        }
    }
}

BOOL WINAPI ProgDlgProc(HWND wnd,unsigned msg,WPARAM wParam,LPARAM lParam)
{
    int i;
    char buffer[128];

    switch(msg)
    {
    case WM_INITDIALOG:
       for(i=0;i<galinfo[gal].pesbytes;i++)
       {
           wsprintf(buffer,"%02X",(unsigned char)pes[i]);
           SetDlgItemText(wnd,110+i,buffer);
       }
       while(i<12)
       {
           ShowWindow(GetDlgItem(wnd,200+i),SW_HIDE);
           ShowWindow(GetDlgItem(wnd,110+i++),SW_HIDE);
       }
       SendMessage(wnd,WM_COMMAND,101,0L);
       return TRUE;
    case WM_COMMAND:
       switch(LOWORD(wParam))
       {
       case IDOK:
           EndDialog(wnd,TRUE);
           return TRUE;
       case IDCANCEL:
           EndDialog(wnd,FALSE);
           return TRUE;
       default:
           for(i=0;i<galinfo[gal].pesbytes;i++)
           {
               if(GetDlgItemText(wnd,110+i,buffer,sizeof(buffer))%3==0||hex(buffer)==-1)
           {
           EnableWindow(GetDlgItem(wnd,IDOK),FALSE);
           return TRUE;
       }
    }
    EnableWindow(GetDlgItem(wnd,IDOK),TRUE);

    for(i=0;i<galinfo[gal].pesbytes;i++)
    {
        GetDlgItemText(wnd,110+i,buffer,sizeof(buffer));
        pes[i]=hex(buffer);
    }

    if ((pes[3]==ATMEL16)||(pes[3]==ATMEL22))
    {
        strcpy(buffer,"  ");
    }
    else
    {
      if(pes[1]&0x10)
      {
          strcpy(buffer,"3.3V");
      }
      else
      {
          strcpy(buffer,"5V");
      }
    }
    switch(pes[3])
    {
    case LATTICE:
        strcat(buffer," Lattice ");
        break;
    case NATIONAL:
        strcat(buffer," National ");
        break;
    case SGSTHOMSON:
        strcat(buffer," ST Microsystems ");
        break;
    case ATMEL16:
    case ATMEL22:
        strcat(buffer, "Atmel ");
    default:
        strcpy(buffer," Unknown ");
    }
    strcat(buffer,galinfo[gal].name);
    ParsePES();
    i=strlen(buffer);
    wsprintf(buffer+i," VPP=%d.%02dV Prog=%dms Erase=%dms",vpp/4,(vpp%4)*25,progtime,erasetime);
    SetDlgItemText(wnd,100,buffer);
    return TRUE;
    }
    }
    return GrayDlgProc(wnd,msg,wParam,lParam);
}

static BOOL TestProperGAL(HWND wnd)
{
    int type=0;

    ReadPES(wnd,pes);
    if (pes[7]=='F' && pes[6]=='2' && pes[5]=='2' && pes[4]=='V' && pes[3]=='1' && pes[2]=='0')
    {
       if (pes[1]=='B')
       {
           type = ATF22V10B;
       }
       else
       {
           type = ATF22V10C;
       }
    }
    else if (pes[6]=='F' && pes[5]=='1' && pes[4]=='6' && pes[3]=='V' && pes[2]=='8')
    {
       type = ATF16V8B;
    }
    else if (pes[2]!=0x00 && pes[2]!=0xFF)
    {
       for(type=sizeof(galinfo)/sizeof(galinfo[0]);type;type--)
       {
           if(pes[2]==galinfo[type].id0||pes[2]==galinfo[type].id1) break;
       }
    }
    if(type==0)
    {
       if(Message(wnd,"Unknown or illegal PES, continue?",
          NULL,MB_YESNO|MB_ICONQUESTION)==IDNO) return FALSE;
    }
    else
    {
       if(type!=gal)
       {
          switch(Message(wnd,"PES indicates a different GAL type than selected. Change to detected GAL type?",NULL,MB_YESNOCANCEL|MB_ICONQUESTION))
             {
          case IDCANCEL:
                 return FALSE;  // cancel = abort
             case IDYES:
                 gal=type;      // yes = change to PES galtype
            SendDlgItemMessage(mainwindow,101,CB_SETCURSEL,gal-1,0L);
              SendMessage(mainwindow,WM_COMMAND,101,MAKELONG(0,CBN_SELCHANGE));
          }                 // no =  continue with current galtype
       }
    }
    ParsePES();
    return TRUE;
}


BOOL WINAPI GALDlgProc(HWND wnd,unsigned msg,WPARAM wParam,LPARAM lParam)
{
    int i,j,l;
    OPENFILENAME ofn;
    WINDOWPLACEMENT wp;
    char *ptr;
    RECT r;
    static RECT editrect;
    int handle;
    char temp[40];

    switch(msg)
    {
    case WM_INITDIALOG:
        mainwindow = wnd;
        GetClientRect(wnd,&r);
        GetWindowRect(GetDlgItem(wnd,102),&editrect);
        ScreenToClient(wnd,(LPPOINT)&editrect.left);
        ScreenToClient(wnd,(LPPOINT)&editrect.right);
        SetRect(&editrect,editrect.left-r.left,editrect.top-r.top,r.right-editrect.right,r.bottom-editrect.bottom);
        if(GetProfileString(progname,"Window","",temp,sizeof(temp))>0)
        {
            sscanf(temp,"%u%u%u%u",&r.left,&r.top,&r.right,&r.bottom);
            MoveWindow(wnd,r.left,r.top,r.right-r.left,r.bottom-r.top,FALSE);
        }
        lpt=GetProfileInt(progname,"Port",1);
        for(i=0;i<3;i++)
        {
            wsprintf(galbuffer,"LPT%d",i+1);
            if(GetProfileString(progname,galbuffer,"",galbuffer,sizeof(galbuffer))>0)
            {
                lptport[i]=strtoul(galbuffer,NULL,0);
            }
            else
            {
                lptport[i]=lptbase[i];
            }
            if(lptport[i])
            {
                wsprintf(galbuffer,"LPT%d: %04x",i+1,lptport[i]);
                j=(int)SendDlgItemMessage(wnd,100,CB_ADDSTRING,0,(LPARAM)(LPSTR)galbuffer);
                SendDlgItemMessage(wnd,100,CB_SETITEMDATA,j,i);
                if(i==lpt) SendDlgItemMessage(wnd,100,CB_SETCURSEL,j,0L);
            }
        }
        SendMessage(wnd,WM_COMMAND,100,MAKELONG(0,CBN_SELCHANGE));
        TurnOff();
        for(i=1;i<sizeof(galinfo)/sizeof(galinfo[0]);i++)
        {
            j=(int)SendDlgItemMessage(wnd,101,CB_ADDSTRING,0,(LPARAM)(LPSTR)galinfo[i].name),
            SendDlgItemMessage(wnd,101,CB_SETITEMDATA,j,galinfo[i].type);
        }
        return TRUE;
    case WM_DESTROY:
        wp.length=sizeof(wp);
        GetWindowPlacement(wnd,&wp);
        sprintf(temp,"%u %u %u %u",wp.rcNormalPosition.left,wp.rcNormalPosition.top,wp.rcNormalPosition.right,wp.rcNormalPosition.bottom);
        WriteProfileString(progname,"Window",temp);
        break;
    case WM_SIZE:
        GetClientRect(wnd,&r);
        MoveWindow(GetDlgItem(wnd,102),r.left+editrect.left,r.top+editrect.top,r.right-editrect.right-(r.left+editrect.left),r.bottom-editrect.bottom-(r.top+editrect.top),TRUE);
        break;
    case WM_COMMAND:
        switch(LOWORD(wParam))
        {
        case IDCANCEL:
            if(SendDlgItemMessage(wnd,102,EM_GETMODIFY,0,0L))
            {
                switch(Message(wnd,"JEDEC fuse map changed, save ?",NULL,
                MB_ICONQUESTION|MB_YESNOCANCEL))
                {
                case IDCANCEL:
                    return 0L;
                case IDYES:
                    if(SendMessage(wnd,WM_COMMAND,4,0L)==0) return 0L;
                }
            }
        EndDialog(wnd,0);
        return TRUE;
    case 3: // Load
        memset(&ofn,0,sizeof(OPENFILENAME));
        galbuffer[0]='\0';
        ofn.lStructSize=sizeof(OPENFILENAME);
        ofn.hwndOwner=wnd;
        ofn.hInstance=hInstance;
        ofn.lpstrFilter="Fusemaps(.JED)\0*.jed\0All Files\0*.*\0";
        ofn.nFilterIndex=1L;
        ofn.lpstrFile=galbuffer;
        ofn.lpstrDefExt="JED";
        ofn.lpstrTitle="Load JEDEC";
        ofn.nMaxFile=sizeof(galbuffer);
        ofn.Flags=OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST|OFN_HIDEREADONLY;
        if(GetOpenFileName(&ofn))
        {
            handle=_lopen(galbuffer,OF_READ);
            if(handle==-1)
            {
                Message(wnd,"Cannot read file",NULL,MB_OK|MB_ICONEXCLAMATION);
            }
        else
        {
            i=_lread(handle,galbuffer,sizeof(galbuffer));
            galbuffer[i]='\0';
            ptr=strchr(galbuffer,'\2');
            if(ptr)
            {
                memmove(galbuffer,ptr+1,i-(ptr-galbuffer));
                ptr=strchr(galbuffer,'\3');
                if(ptr) *ptr='\0';
            }
            SetDlgItemText(wnd,102,galbuffer);
            _lclose(handle);
            SendMessage(wnd,WM_COMMAND,102,MAKELONG(0,EN_CHANGE));
            i=(int)SendDlgItemMessage(wnd,101,CB_GETCURSEL,0,0L);
            if(i==-1) gal=0; else gal=(int)SendDlgItemMessage(wnd,101,CB_GETITEMDATA,i,0L);
            CheckJEDEC(wnd);
        }
    }
    break;
    case 4: //  Save
        if(SendDlgItemMessage(wnd,102,WM_GETTEXTLENGTH,0,0L)<1)
        {
            Message(wnd,"Nothing to save",progname,MB_ICONEXCLAMATION|MB_OK);
            return 0L;
        }
        memset(&ofn,0,sizeof(OPENFILENAME));
        galbuffer[0]='\0';
        ofn.lStructSize=sizeof(OPENFILENAME);
        ofn.lpstrTitle="Save JEDEC";
        ofn.hwndOwner=wnd;
        ofn.hInstance=hInstance;
        ofn.lpstrFilter="Fusemaps(.JED)\0*.jed\0All Files\0*.*\0";
        ofn.nFilterIndex=1L;
        ofn.lpstrFile=galbuffer;
        ofn.lpstrDefExt="JED";
        ofn.nMaxFile=sizeof(galbuffer);
        ofn.Flags=OFN_PATHMUSTEXIST|OFN_PATHMUSTEXIST|OFN_HIDEREADONLY;
        if(GetSaveFileName(&ofn))
        {
            handle=_lcreat(galbuffer,0);
            if(handle==-1)
            {
                Message(wnd,"Can not create from file",NULL,MB_OK|MB_ICONEXCLAMATION);
            }
            else
            {
                galbuffer[0]='\2';
                i=GetDlgItemText(wnd,102,galbuffer+1,sizeof(galbuffer)-5)+1;
                for(l=0,j=1;j<i;j++) l+=galbuffer[j];
                i+=wsprintf(galbuffer+i,"\3%04X",l+5);
                _lwrite(handle,galbuffer,i);
                _lclose(handle);
                SendDlgItemMessage(wnd,102,EM_SETMODIFY,0,0L);
                return TRUE;
            }
        }
        break;
    case 5: // Read GAL
        if(!TestProperGAL(wnd)) return TRUE; // read PES, check that it matches selected GAL.
        ReadGAL(wnd,fusemap);                // read fuse rows, UES, cfg.
        FormatJEDEC(gal,fusemap,galbuffer);
        SetDlgItemText(wnd,102,galbuffer);
        SendMessage(wnd,WM_COMMAND,102,MAKELONG(0,EN_CHANGE));
        return TRUE;
    case 6: // Write GAL
        if(GetDlgItemText(wnd,102,galbuffer,sizeof(galbuffer))<1)
        {
            MessageBox(wnd,"Load a JEDEC fuse map first",progname,MB_ICONEXCLAMATION|MB_OK);
            return 0L;
        }
        if(!CheckJEDEC(wnd)) return TRUE;
        if(!TestProperGAL(wnd)) return TRUE;
        WriteGAL(wnd,fusemap);
        if(security)
        {
            if(Message(wnd,"Programming the security fuse will prohibit the readout and verification of the GAL. Do you want to continue ?",progname,MB_ICONEXCLAMATION|MB_YESNO)!=IDYES) return TRUE;
            BurnSecurity(wnd,gal);
        }
        return TRUE;
    case 12: // Verify GAL
        i=(int)SendDlgItemMessage(wnd,101,CB_GETCURSEL,0,0L);
        if(GetDlgItemText(wnd,102,galbuffer,sizeof(galbuffer))<1)
        {
            MessageBox(wnd,"Load a JEDEC fuse map first",progname,MB_ICONEXCLAMATION|MB_OK);
            return 0L;
        }
        if(!CheckJEDEC(wnd)) return TRUE;
        if(!TestProperGAL(wnd)) return TRUE;
        ReadGAL(wnd,backup);
        if(memcmp(backup,fusemap,galinfo[gal].fuses)==0)
        {
            Message(wnd,"Verify ok.",NULL,MB_OK);
        }
        else
        {
            Message(wnd,"Verify failed!",NULL,MB_OK);
        }
        return TRUE;
    case 7: // Erase GAL
        if(!TestProperGAL(wnd)) return TRUE;
        if(pes[1]&0x80) if(Message(wnd,"GAL is a MASTER, still erase?",NULL,MB_ICONSTOP|MB_YESNO)!=IDYES) return TRUE;
        EraseGAL(wnd,gal);
        return TRUE;
    case 8: // Write PES
        if(!GetSetup(wnd)) return FALSE;
        if(DialogBox(hInstance,MAKEINTRESOURCE(2),wnd,ProgDlgProc))
        {
            WritePES(wnd,pes);
        }
        return TRUE;
    case 10: // SECURITY
        if(!TestProperGAL(wnd)) return TRUE;
        if(Message(wnd,"Programming the security fuse will prohibit the readout and verification of the GAL. Do you want to continue ?",progname,MB_ICONEXCLAMATION|MB_YESNO)!=IDYES) return TRUE;
        BurnSecurity(wnd,gal);
        return TRUE;
    case 11: // ERASE ALL
        if(!TestProperGAL(wnd)) return TRUE;
        if(Message(wnd,"Erase the whole GAL will also erase the PES (Programmer Electronic Signature) rendering the GAL unusable. Do you want to continue ?",progname,MB_ICONEXCLAMATION|MB_YESNO)!=IDYES) return TRUE;
        EraseWholeGAL(wnd,gal);
        return TRUE;
    case 100:
        if(HIWORD(lParam)==CBN_SELCHANGE)
        {
            i=(int)SendDlgItemMessage(wnd,100,CB_GETCURSEL,0,0L);
            if(i!=-1)
            {
                lpt=(int)SendDlgItemMessage(wnd,100,CB_GETITEMDATA,i,0L);
                itoa(lpt,galbuffer,10);
                WriteProfileString(progname,"Port",galbuffer);
                TurnOff();
            }
        }
        return TRUE;
    case 101:
        if(HIWORD(lParam)==CBN_SELCHANGE)
        {
            i=(int)SendDlgItemMessage(wnd,101,CB_GETCURSEL,0,0L);
            if(i==-1) gal=0; else gal=(int)SendDlgItemMessage(wnd,101,CB_GETITEMDATA,i,0L);
        }
        return TRUE;
    case 900:
        i=GetModuleFileName(hInstance,galbuffer,sizeof(galbuffer));
        strcpy(galbuffer+i-3,"htm");
        if(ShellExecute(wnd,"open",galbuffer,NULL,NULL,SW_SHOW)<32)
        {
            MessageBox(wnd,galbuffer,"File not found",MB_OK|MB_ICONEXCLAMATION);
        }
        return TRUE;
    case 999:
        return DialogBox(hInstance,MAKEINTRESOURCE(3),wnd,AboutDlgProc);
    case 200:
    case 201:
    case 202:
    case 203:
    case 204:
    case 205:
    case 206:
    case 207:
    case 208:
    case 209:
        SendDlgItemMessage(wnd,101,CB_SETCURSEL,LOWORD(wParam)-200,0L);
        return TRUE;
    case 300:
    case 301:
    case 302:
        SendDlgItemMessage(wnd,100,CB_SETCURSEL,LOWORD(wParam)-300,0L);
        return TRUE;
    case 310:
        return TRUE;
    }
    break;
    case WM_INITMENU:
        i=(int)SendDlgItemMessage(wnd,100,CB_GETCURSEL,0,0L);
        return 0L;
    }
    return GrayDlgProc(wnd,msg,wParam,lParam);
}

int PASCAL WinMain(HANDLE hInst,HANDLE hPrev,LPSTR lpCmdLine,int nCmdShow)
{
    hInstance=hInst;
    vppmul=GetProfileInt(progname,"MulDiv",128);
    appicon=LoadIcon(hInst,MAKEINTRESOURCE(1));
    return DialogBox(hInst,MAKEINTRESOURCE(1),0,GALDlgProc);
}

